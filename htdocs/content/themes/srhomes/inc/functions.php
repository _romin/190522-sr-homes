<?php

use Theme\Walkers\NavigationWalker;
use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\Filter;

/**
 * Enqueue Fontawesome 
 */
Asset::add('font-awesome-free', '//use.fontawesome.com/releases/v5.8.1/css/all.css', [], '5.8.1', 'all')->to('front');

/**
 * Enqueue JQuery 
 */
if(Request::is('properties/*')) {

    Asset::add('google-maps-acf', 'js/google-maps-acf.js', ['jquery'], '1.0.0', true)->to('front');
    Asset::add('google-maps', '//maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAPS_KEY'), ['google-maps-acf'], '3.22', true)
        ->setType('script')
        ->to('front');
};

if(Request::is('/')) {

    Asset::add('google-maps-init', 'js/google-maps-init.js', ['jquery'], '1.0.0', true)->to('front');
    Asset::add('google-maps', '//maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAPS_KEY') . '&callback=initMap', ['google-maps-init'], '3.22', true)
        ->setType('script')
        ->to('front');
}

/**
 * Remove custom actions from the header
 */
Action::remove('wp_head', 'print_emoji_detection_script', 7);
Action::remove('wp_print_styles', 'print_emoji_styles');
Action::remove( 'admin_print_scripts', 'print_emoji_detection_script' );
Action::remove( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Remove admin menu URLs.
 */
Action::add('admin_menu', function () {
    remove_menu_page('edit.php');
    remove_menu_page( 'edit-comments.php' );
});

/**
 * ACF Google maps API key
 */
Filter::add('acf/init', function () {
    acf_update_setting('google_api_key', 'AIzaSyA1zDMzQyQuSC2dikR4tJ12bG4Pw-8Z3Hw');
});

/**
 * Remove Wordpress 28px from admin bar.
 */
Filter::add('admin_bar_init', function () {
    return Action::remove('wp_head', '_admin_bar_bump_cb');
});

/**
 * Remove automatic redirect.
 */
Filter::remove('template_redirect', 'redirect_canonical');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
Action::add('after_setup_theme', function () {
    $GLOBALS['content_width'] = 640;
    add_theme_support( 'post-thumbnails' );
}, 0);

/**
 * Add ACF options page.
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

/**
 * Save ACF json file.
 */
Filter::add('acf/settings/save_json', function ($paths) {
    return get_stylesheet_directory() . '/acf-json';
});
  
/**
 * Load ACF json file.
 */
Filter::add('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf-json';
    return $paths;
});

/**
 * Set header menu left with a custom walker.
 */
Action::add('wp_header_menu', function () {
    return wp_nav_menu([
        'theme_location' => 'header-menu',
        'walker' => new NavigationWalker(
            ['nav__item'],
            ['nav__sub-menu']
        ),
        'container' =>  'div',
        'items_wrap'=> '%3$s',
        'container_class' => 'nav__menu',
    ]);
});