@include('partials.cookies')

<footer class="footer">
    <div class="footer__links">
        <div class="container">
            <div class="grid">
                <div class="grid__col-3">
                    @if( have_rows('footer_cta', 'options') )
                        <ul class="menu">
                            @while ( have_rows('footer_cta', 'options') )
                                @php(the_row())

                                <li class="menu__link">
                                    <a href="{{ get_sub_field('link')['url'] }}" target="{{ get_sub_field('link')['target'] }}">
                                    {!! get_sub_field('link')['title'] !!}
                                    </a>
                                </li>
                            @endwhile
                        </ul>
                    @endif
                </div>
                <div class="grid__col-3">
                    <ul class="menu">
                        @if(get_field('facebook', 'options'))
                            <li class="menu__link">
                                <a href="{{ get_field('facebook', 'options') }}" target="_blank">Facebook</a>
                            </li>
                        @endif
                        @if(get_field('twitter', 'options'))
                            <li class="menu__link">
                                <a href="{{ get_field('twitter', 'options') }}" target="_blank">Twitter</a>
                            </li>
                        @endif
                        @if(get_field('google', 'options'))
                            <li class="menu__link">
                                <a href="{{ get_field('google', 'options') }}" target="_blank">Google</a>
                            </li>
                        @endif
                        @if(get_field('linked_in', 'options'))
                            <li class="menu__link">
                                <a href="{{ get_field('linked_in', 'options') }}" target="_blank">LinkedIn</a>
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="grid__col-3">
                    @if( have_rows('address_information', 'options') )
                        <div class="menu menu--icon">
                            <div class="menu__icon">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <ul class="menu__description">
                                @while ( have_rows('address_information', 'options') )
                                    @php(the_row())
                                    <li></li>{{ the_sub_field('address_line') }}<li>
                                @endwhile
                            </ul>
                        </div>
                    @endif
                    
                    @if(get_field('phone_number', 'options'))
                        <div class="menu menu--icon">
                            <div class="menu__icon">
                                <i class="fas fa-phone"></i>
                            </div>
                            <ul class="menu__description">
                                <li>
                                    <a href="tel:{{ get_field('phone_number', 'options') }}">{{ get_field('phone_number', 'options') }}</a>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <div class="container">
            <div class="grid">
                <div class="grid__col-2">
                    @if(get_field('copyright', 'options'))
                        {{ get_field('copyright', 'options') }}
                    @endif
                </div>
                <div class="grid__col-2">
                    Developed by <a href="https://uk.linkedin.com/in/rominpatel1" target="_blank">Romin</a>
                </div>
            </div>
        </div>
    </div>
</footer>