<header class="header">
    <div class="header__container">
        <a href="/" class="header__logo">
            <div class="heading">SR</div>
            <div class="sub-heading">Homes</div>
        </a>
        <div class="nav">
            <div class="nav__container">
                {!! do_action('wp_header_menu') !!}

                <div class="hamburger">
                    <div class="hamburger__menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

<div class="spacer"></div>