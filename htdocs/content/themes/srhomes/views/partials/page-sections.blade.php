@if( have_rows('page_sections') )
    @while ( have_rows('page_sections') ) @php the_row() @endphp

        @if( get_row_layout() == 'hero' )
            @include('sections.hero')
        @endif

        @if( get_row_layout() == 'properties' )
            @include('sections.properties')
        @endif

        @if( get_row_layout() == 'dream_home' )
            @include('sections.dream-home')
        @endif

        @if( get_row_layout() == 'testimonials' )
            @include('sections.testimonials')
        @endif

        @if( get_row_layout() == 'articles' )
            @include('sections.articles')
        @endif

        @if( get_row_layout() == 'map' )
            @include('sections.map')
        @endif

    @endwhile
@endif