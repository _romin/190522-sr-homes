<div class="cookies">
    <div class="column">
        <div class="column__content">
            <div class="cookies__message">
                We use cookies to ensure that we give you the best experience on our website.
            </div>
        </div>
        <div class="column__button">
        <span class="cookies__button">
            Accept & Close
        </span>
        </div>
    </div>
</div>