@if( have_rows('hero') )
    <div class="section-hero">
        @while ( have_rows('hero') ) @php the_row() @endphp
            <div>
                <div class="section-hero__slide" style="background-image: url({{ get_sub_field('background_image')['url'] }})">
                    <div class="container">
                        <div class="section-hero__inner">
                            @if(! get_sub_field('only_image'))

                                @if(get_sub_field('description'))
                                    <h1 class="section-hero__heading">
                                        {!! get_sub_field('description') !!}
                                    </h1>
                                @endif

                                @if(get_sub_field('button'))
                                    <a href="{{ get_sub_field('button')['url'] }}" target="{{ get_sub_field('button')['target'] }}" class="button button--large button--yellow section-hero__button">
                                        {{ get_sub_field('button')['title'] }}
                                    </a>

                                @endif

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endwhile
    </div>
@endif