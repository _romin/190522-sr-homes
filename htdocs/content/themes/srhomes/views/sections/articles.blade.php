<div class="section-articles">
    @if(get_sub_field('heading'))
        <div class="heading">
            <h2 class="heading__content">
                {{ get_sub_field('heading') }}
            </h2>
            <span class="heading__arrows"></span>
        </div>
    @endif

    <div class="section-articles__wrapper">
        <div class="grid grid--center">
            @query(['post_type' => 'articles', 'posts_per_page' => 3])
                @php
                    $image = get_the_post_thumbnail_url(get_the_ID(), 'full');
                    $placeholder = get_field('placeholder_image', 'option')['sizes']['medium_large'];
                @endphp
                <div class="grid grid__col-3">

                    <a class="card" href="{{ get_permalink() }}">
                        <div class="card__image" style="background-image: url({{ ($image) ? $image : $placeholder }})"></div>
                        <div class="card__content">
                            @php
                                $categories = wp_get_post_terms(get_the_ID(), 'categories', array( 'fields' => 'all' ) )
                            @endphp
                            @if(count($categories))
                                <div class="card__row">
                                    @foreach($categories as $index => $category)
                                        @php
                                            $index++
                                        @endphp
                                        <span class="article__category">{{ $category->name }}@if($index < count($categories)), @endif</span>
                                    @endforeach
                                </div>
                            @endif
                            <div class="card__row">
                                <h3 class="article__heading">{{ get_the_title() }}</h3>
                            </div>
                            <div class="card__row card__row--equal">
                                <div class="article__content">
                                    {{ wp_trim_words( get_the_content(), 30, '...' ) }}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endquery
        </div>

        <a href="{{ route('articles.index')  }}" target="_blank" class="section-articles__button">
            View All Articles
        </a>

    </div>
</div>