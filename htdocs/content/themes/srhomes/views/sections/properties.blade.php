<div class="section-properties">

    <div class="container">
        @if(get_sub_field('heading'))
            <h2 class="h2--display">
                {{ get_sub_field('heading') }}
            </h2>
        @endif
        @if(get_sub_field('description'))
            <div class="section-properties__description">
                {!! get_sub_field('description') !!}
            </div>
        @endif
    </div>

    @if( is_array(get_sub_field('properties')) )
        <div class="container">
            <div class="section-properties__slides">
                @foreach(get_sub_field('properties') as $property)
                    @php
                        $id = $property->ID
                    @endphp

                        @php
                            $image = get_field('property_images', $id)[0]['image']['sizes']['medium_large'];
                            $placeholder = get_field('placeholder_image', 'option')['sizes']['medium_large'];
                        @endphp

                        <a href="{{ get_permalink($id) }}" class="card card--image property">
                            <div class="card__image"
                                    style="background-image: url({{ ($image) ? $image : $placeholder }})">
                            </div>
                            <div class="card__content">
                                <div class="card__row">
                                    @if(get_field('sale_price', $id) && get_field('rent_price', $id))
                                        @if(get_field('property_for', $id) == 'sale')
                                            <span class="property__price">£{{ number_format(get_field('sale_price', $id)) }}</span>
                                        @else
                                            <span class="property__price">£{{ number_format(get_field('rent_price', $id)) }}</span>
                                        @endif
                                    @endif
                                </div>
                                <div class="card__row card__row--equal">
                                    <h3 class="property__location">{{ get_the_title($id) }}</h3>
                                </div>
                            </div>
                        </a>
                @endforeach
            </div>

            @if(count(get_sub_field('properties')) > 3)
                <div class="carousel__controls">
                    <div class="carousel__col-2 carousel__col-2--left">
                        <div class="carousel__prev carousel__prev--dark carousel__prev-properties">
                            <i class="fas fa-chevron-left"></i>
                        </div>
                    </div>
                    <div class="carousel__col-2 carousel__col-2--right">
                        <div class="carousel__next carousel__next--dark carousel__next-properties">
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    @endif
</div>