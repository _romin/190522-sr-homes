<div class="section-search-properties header__logo-spacer">

    <div class="container">

        <form action="{{ route('property.index') }}" method="GET" class="form">

            <div class="form__field">
                <label class="form__label">Location</label>
                <input type="text" class="form__input" name="location" placeholder="Street, City, Postcode" value="{{ request()->location }}">
            </div>

            <div class="form__field">
                <label class="form__label">For</label>
                <select name="for" class="form__input">
                    <option value="sale" @if(request()->for == 'sale') selected @endif>Sale</option>
                    <option value="rent" @if(request()->for == 'rent') selected @endif>Rent</option>
                </select>
            </div>

            <div class="form__field">
                <label class="form__label">Bedrooms</label>
                <select name="bedrooms" class="form__input">
                    @for ($i = 1; $i <= 10; $i++)
                        <option value="{{ $i }}" @if(request()->bedrooms == $i) selected @endif>{{ $i }}</option>
                    @endfor
                </select>
            </div>

            <div class="form__field">
                <label class="form__label">Max Price</label>
                <input type="number" class="form__input" name="max" value="{{ request()->max }}" onKeyPress="if(this.value.length === 7) return false;">
            </div>

            <div class="form__field form__field--auto-width">
                <button type="submit" class="button form__button">Submit</button>
            </div>

        </form>
    </div>

</div>