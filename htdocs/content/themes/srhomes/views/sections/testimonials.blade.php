<div class="section-testimonials">

    @if(get_sub_field('heading'))
        <div class="container">
            <h2 class="h2">
                {{ get_sub_field('heading') }}
            </h2>
        </div>
    @endif

    <div class="section-testimonials__slides">
        @query(['post_type' => 'testimonials', 'posts_per_page' => 4])
            <div class="testimonial">
                <div class="container">
                    @if(get_field('testimonial'))
                        <div class="testimonial__quote">
                            {!! get_field('testimonial') !!}
                        </div>
                    @endif
                    @if(get_field('name'))
                        <div class="testimonial__by">
                            {{ get_field('name') }}
                        </div>
                    @endif
                </div>
            </div>
        @endquery
    </div>
    @if(count($testimonials->posts) > 1)
        <div class="carousel__controls">
            <div class="carousel__col-2 carousel__col-2--left">
                <div class="carousel__prev carousel__prev--dark carousel__prev-testimonials">
                    <i class="fas fa-chevron-left"></i>
                </div>
            </div>
            <div class="carousel__col-2 carousel__col-2--right">
                <div class="carousel__next carousel__next--dark carousel__next-testimonials">
                    <i class="fas fa-chevron-right"></i>
                </div>
            </div>
        </div>
    @endif
</div>