<div class="section-dream-home">
    @if(get_sub_field('heading'))
        <div class="container">
            <h2 class="h2--display">
                {{ get_sub_field('heading') }}
            </h2>
        </div>
    @endif

    <div class="container">
        <div class="grid">
            <div class="grid__col-2">
                <div class="image" style="background-image: url({{ get_sub_field('image')['sizes']['medium_large'] }});"></div>
            </div>
            <div class="grid__col-2">
                <div class="search-properties">

                    <ul class="tabs">
                        <li class="tabs__link current" data-tab="buy-tab">Buy</li>
                        <li class="tabs__link" data-tab="rent-tab">Rent</li>
                    </ul>

                    <div id="buy-tab" class="tab__content current">

                        <form action="{{ route('property.index') }}" method="GET" class="form">
                            <input type="hidden" name="for" value="sale">

                            <div class="form__field">
                                <label class="form__label">Location</label>
                                <input name="location" class="form__input" type="text" placeholder="Street, City, Postcode">
                            </div>

                            <div class="form__field">
                                <label class="form__label">Bedrooms</label>
                                <select name="bedrooms" class="form__input">
                                    @for ($i = 1; $i <= 10; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form__field">
                                <label class="form__label">House Type</label>
                                <select name="type" class="form__input">
                                    <option value="house">House</option>
                                    <option value="flat">Flat</option>
                                </select>
                            </div>

                            <div class="form__field">
                                <label class="form__label">Max Price</label>
                                <div class="range-slider">
                                    <input class="range-slider__range" type="range" name="max" value="250000" min="0" max="1000000" step="50000">
                                    <span class="range-slider__value">0</span>
                                </div>
                            </div>

                            <div class="form__field form__field--auto-width">
                                <button type="submit" class="button button--large button--blue-bordered form__button">Search Properties on Sale</button>
                            </div>
                        </form>

                    </div>

                    <div id="rent-tab" class="tab__content">

                        <form action="{{ route('property.index') }}" method="GET" class="form">
                            <input type="hidden" name="for" value="rent">

                            <div class="form__field">
                                <label class="form__label">Location</label>
                                <input name="location" class="form__input" type="text" placeholder="Street, City, Postcode">
                            </div>

                            <div class="form__field">
                                <label class="form__label">Bedrooms</label>
                                <select name="bedrooms" class="form__input">
                                    @for ($i = 1; $i <= 10; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form__field">
                                <label class="form__label">House Type</label>
                                <select name="type" class="form__input">
                                    <option value="house">House</option>
                                    <option value="flat">Flat</option>
                                </select>
                            </div>

                            <div class="form__field">
                                <label class="form__label">Max Price</label>
                                <div class="range-slider">
                                    <input class="range-slider__range" type="range" name="max" value="1500" min="0" max="3000" step="300">
                                    <span class="range-slider__value">0</span>
                                </div>
                            </div>

                            <div class="form__field form__field--auto-width">
                                <button type="submit" class="button button--large button--blue-bordered form__button">Search Properties on Rent</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>