<!doctype html>
<html {!! get_language_attributes() !!}>
<head>
    <meta charset="{{ get_bloginfo('charset') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="{{ get_stylesheet_directory_uri() }}/dist/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{ get_stylesheet_directory_uri() }}/dist/images/favicon.png" type="image/x-icon">

    @head

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PZNFSVL');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZNFSVL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">

    @include('partials.header')

    @yield('content')

    @include('partials.footer')


    @if(get_field('schema_json'))
        {!! get_field('schema_json') !!}
    @endif

    @if(get_field('global_schema_json', 'options'))
        {!! get_field('global_schema_json', 'options') !!}
    @endif

@footer
</div>
</body>
</html>
