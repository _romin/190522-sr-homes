@extends('layouts.main')

@section('content')
    <div class="error-404">
        <div class="container container--medium">

            <div class="error-404__code">
                404
            </div>

            <div class="information">
                @if(get_field('error_page_information_heading', 'options'))
                    <h1 class="information__heading">
                        {{ get_field('error_page_information_heading', 'options') }}
                    </h1>
                @endif
                @if(get_field('error_page_information_heading', 'options'))
                    <div class="information__sub-heading">
                        {!! get_field('error_page_information_sub_heading', 'options') !!}
                    </div>
                @endif

                    @if( have_rows('error_page_cta', 'options') )
                        <p>Here are some helpful links instead.</p>
                        <ol class="cta">
                            @while ( have_rows('error_page_cta', 'options') )
                                @php(the_row())
                                <li class="cta__link">
                                    <a href="{{ get_sub_field('link')['url'] }}" class="cta__link" target="{{ get_sub_field('link')['target'] }}">{{ get_sub_field('link')['title'] }}.</a>
                                </li>
                            @endwhile
                        </ol>
                    @endif
            </div>
        </div>

    </div>
    </div>
@endsection