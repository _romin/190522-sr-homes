@extends('layouts.main')

@section('content')
    <div class="article">
        <div class="article__wrapper">
            @if($article->have_posts())
                @while($article->have_posts()) @php($article->the_post())

                <div class="article__heading">
                    {{ get_the_title() }}
                </div>

                <div class="article__information">
                    Posted By {{ ucfirst(get_the_author()) }}, {{ get_the_date() }}
                </div>

                @php($image = get_the_post_thumbnail_url(get_the_ID(), 'full'))
                @php($placeholder = get_field('placeholder_image', 'option')['sizes']['medium_large'])

                <div class="article__image" style="background-image: url({{ ($image) ? $image : $placeholder }})"></div>

                <div class="article__content">
                    {!! the_content() !!}
                </div>
                @endwhile
            @endif
        </div>
    </div>
@endsection