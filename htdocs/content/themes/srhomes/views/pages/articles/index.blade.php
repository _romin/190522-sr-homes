@extends('layouts.main')

@section('content')

    <div class="articles">
        <div class="articles__wrapper">
            <div class="grid">

                @if($articles->have_posts())
                    @while($articles->have_posts()) @php($articles->the_post())

                    @php($image = get_the_post_thumbnail_url(get_the_ID(), 'full'))
                    @php($placeholder = get_field('placeholder_image', 'option')['sizes']['medium_large'])

                    <div class="grid grid__col-3">
                        <a class="card" href="{{ get_permalink() }}">
                            <div class="card__image"
                                 style="background-image: url({{ ($image) ? $image : $placeholder }})"></div>
                            <div class="card__content">
                                @php($categories = wp_get_post_terms(get_the_ID(), 'categories', array('fields' => 'all')))

                                @if(count($categories))
                                    <div class="card__row">
                                        @foreach($categories as $index => $category)
                                            @php($index++)
                                            <span class="article__category">{{ $category->name }}@if($index < count($categories)), @endif</span>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="card__row">
                                    <h3 class="article__heading">{{ get_the_title() }}</h3>
                                </div>
                                <div class="card__row card__row--equal">
                                    <div class="article__content">
                                        {{ wp_trim_words( get_the_content(), 30, '...' ) }}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endwhile
                @else
                    Sorry, no posts found.
                @endif
            </div>

            {!! paginate_links([
                 'format' => '?page=%#%',
                 'current' => max( 1, get_query_var('page') ),
                 'posts_per_page' => 1,
                 'total' => $articles->max_num_pages
             ]) !!}
        </div>
    </div>
@endsection