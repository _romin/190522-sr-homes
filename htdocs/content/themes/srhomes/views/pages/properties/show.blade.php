@extends('layouts.main')

@section('content')
    <div class="single-property">
        <div class="container">

            @if($property->have_posts())
                @while($property->have_posts()) @php($property->the_post())

                <div class="grid">
                    <div class="grid__col-1 grid__col-1--sticky">
                        <div class="single-property__sidenav">
                            <h1 class="single-property__location">
                                {{ get_the_title() }}
                            </h1>
                            @if(get_field('property_for') == 'sale')
                                @if(get_field('sale_price'))
                                    <span class="single-property__price">£{{ number_format(get_field('sale_price')) }}</span>
                                @endif
                            @else
                                @if(get_field('rent_price'))
                                    <span class="single-property__price">£{{ number_format(get_field('rent_price')) }} pcm</span>
                                @endif
                            @endif

                            @if(get_field('show_additional_information'))
                                <div class="single-property__additional-information">
                                    {!! get_field('additional_information') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="grid__col-2">
                        @if( have_rows('property_images') )
                            <div class="single-property__slider">
                                <div class="single-property__slides">
                                    @foreach(get_field('property_images') as $attachment )
                                        <div class="single-property__image"
                                             style="background-image: url({{ $attachment['image']['sizes']['large']  }})"></div>
                                    @endforeach
                                </div>

                                @if(count(get_field('property_images')) > 1)
                                    <div class="carousel__controls">
                                        <div class="carousel__col-2 carousel__col-2--left">
                                            <div class="carousel__prev carousel__prev--dark carousel__prev carousel__prev-single-property">
                                                <i class="fas fa-chevron-left"></i>
                                            </div>
                                        </div>
                                        <div class="carousel__col-2 carousel__col-2--right">
                                            <div class="carousel__next carousel__next--dark carousel__next carousel__next-single-property">
                                                <i class="fas fa-chevron-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        @endif

                        <div class="single-property__content">
                            <ul class="tabs">
                                <li class="tabs__link current" data-tab="description-tab">Description</li>
                                <li class="tabs__link" data-tab="property-tab">Property</li>
                                <li class="tabs__link" data-tab="map-tab">Map</li>
                            </ul>
                            <div id="description-tab" class="tab__content current">
                                <div class="single-property__description">
                                    {!! the_content() !!}

                                    <div class="single-property__additional-information--tablet">
                                        @if(get_field('show_additional_information'))
                                            <h2 class="h2">Additional Information</h2>
                                            {!! get_field('additional_information') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div id="property-tab" class="tab__content">
                                <div class="single-property__description">
                                    <h2 class="h2">Property Information</h2>
                                    <ul>
                                        <li>
                                            @if(get_field('property_type'))
                                                {{ ucfirst(get_field('property_type')) }}
                                            @endif

                                            @if(get_field('property_type'))
                                                for {{ get_field('property_for') }}
                                            @endif
                                        </li>
                                        <li>
                                            @if(get_field('bathrooms'))
                                                {{ get_field('bathrooms') }}

                                                @if(get_field('bathrooms') != 1)
                                                    bathrooms
                                                @else
                                                    bathroom
                                                @endif
                                            @endif
                                        </li>
                                        <li>
                                            @if(get_field('bedrooms'))
                                                {{ get_field('bedrooms') }}

                                                @if(get_field('bedrooms') != 1)
                                                    bedrooms
                                                @else
                                                    bedroom
                                                @endif
                                            @endif
                                        </li>
                                    </ul>

                                    @if( have_rows('room_sizes') )
                                        <h2 class="h2">Room Sizes</h2>
                                        <ul>
                                            @while ( have_rows('room_sizes') ) @php(the_row())
                                            <li>{{ get_sub_field('room') }} - {{ get_sub_field('size') }}</li>
                                            @endwhile
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <div id="map-tab" class="tab__content">
                                @if( !empty(get_field('map')) )
                                    <div class="acf-map">
                                        <div class="marker" data-lat="{{ get_field('map')['lat'] }}" data-lng="{{ get_field('map')['lng'] }}"></div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endwhile
            @else
                Sorry, no properties found.
            @endif
        </div>
    </div>
@endsection