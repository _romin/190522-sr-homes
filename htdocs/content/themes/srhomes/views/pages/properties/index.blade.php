@extends('layouts.main')

@section('content')
    @include('sections.search-properties')

    <div class="properties">
        <div class="container">

            @if($properties->have_posts())
                @while($properties->have_posts()) @php($properties->the_post())

                @php($image = get_field('property_images')[0]['image']['sizes']['medium_large'])
                @php($placeholder = get_field('placeholder_image', 'option')['sizes']['medium_large'])

                <a href="{{ get_permalink() }}" class="property">
                    <div class="card card--horizontal">
                        <div class="card__image"
                             style="background-image: url({{ ($image) ? $image : $placeholder }})"></div>
                        <div class="card__content">
                            <div class="card__row">
                                @if(get_field('property_for') == 'sale')
                                    @if(get_field('sale_price'))
                                        <span class="property__price">£{{ number_format(get_field('sale_price')) }}</span>
                                    @endif
                                @else
                                    @if(get_field('rent_price'))
                                        <span class="property__price">£{{ number_format(get_field('rent_price')) }} pcm</span>
                                    @endif
                                @endif
                            </div>
                            <div class="card__row">
                                <h4 class="property__type">
                                    @if(get_field('bedrooms'))
                                        {{ get_field('bedrooms') }}

                                        @if(get_field('bedrooms') != 1)
                                            bedrooms,
                                        @else
                                            bedroom,
                                        @endif
                                    @endif

                                    @if(get_field('property_type'))
                                        {{ ucfirst(get_field('property_type')) }}
                                    @endif
                                </h4>
                            </div>
                            <div class="card__row">
                                <h2 class="property__location">{{ get_the_title() }}</h2>
                            </div>
                            <div class="card__row card__row--equal">
                                <div class="property__description">
                                    {!!  wpautop(wp_trim_words(get_the_content(), 50, '...' )) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endwhile
            @else
                Sorry, no properties found.
            @endif

            {!! paginate_links([
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('page') ),
                'posts_per_page' => 1,
                'total' => $properties->max_num_pages
            ]) !!}
        </div>
    </div>
@endsection