@extends('layouts.main')

@section('content')
    <div class="container container--large">
        <div class="page">
            @if(have_posts())
                @while(have_posts()) @php(the_post())

                <h1>
                    {{ get_the_title() }}
                </h1>
       
                {!! the_content() !!}
                @endwhile
            @endif
        </div>
    </div>
@endsection