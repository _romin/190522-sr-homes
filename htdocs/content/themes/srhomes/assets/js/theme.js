import $ from 'jquery';

import 'jquery.cookie'

import 'slick-carousel';

$(() => {

    /*
        Toggle mobile menu.
    */
    $('.hamburger__menu').click(function() {
        $(this).toggleClass('hamburger__menu--close');
        $('.nav__menu').toggleClass('nav__menu--open');
    });

    /*
        Properties slider.
    */
    $('.single-property__slides').slick({
        autoplay: false,
        autoplaySpeed: 4500,
        dots: false,
        arrows: true,
        rows: 0,
        prevArrow: $(".carousel__prev-single-property"),
        nextArrow: $(".carousel__next-single-property"),
    });

    /*
        Testimonials slider.
    */
    $('.section-testimonials__slides').slick({
        autoplay: true,
        autoplaySpeed: 4500,
        dots: false,
        arrows: true,
        prevArrow: $(".carousel__prev-testimonials"),
        nextArrow: $(".carousel__next-testimonials"),
    });

    /*
        Properties slider.
    */
    $('.section-properties__slides').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        centerMode: false,
        variableWidth: true,
        infinite: false,
        prevArrow: $(".carousel__prev-properties"),
        nextArrow: $(".carousel__next-properties"),
        rows: 0,
    });

    /*
        Number format.
    */
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    /*
        Range slider.
    */
    var slider = $('.range-slider');
    var range = $('.range-slider__range');
    var value = $('.range-slider__value');

    slider.each(function(){
        value.each(function(){
            var value = $(this).prev().attr('value');
            $(this).html( '£' + formatNumber(value));
        });

        range.on('input', function(){
            $(this).next(value).html('£' + formatNumber(this.value));
            $(this).next(value).attr('value', '£' + formatNumber(this.value));
        });
    });

    /*
        Tabs.
    */
    $('.tabs .tabs__link').click(function(){
        var tabID = $(this).attr('data-tab');

        $('.tabs .tabs__link').removeClass('current');
        $('.tab__content').removeClass('current');

        $(this).addClass('current');
        $('#' + tabID).addClass('current');
    });

    /*
        Smooth Scroll.
    */
    $.fn.smoothSectionScroll = function()
    {
        // wait till page loads
        setTimeout(function ()
        {
            let hash = window.location.hash.replace('#', '.');

            if(window.location.hash) {
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                }, 700);
            }

        }, 400);

        // Stop all scrolling if the user scrolls half way.
        $('html, body').bind('scroll mousedown DOMMouseScroll mousewheel keyup', function(){
            $('html, body').stop();
        });
    };

    /*
        When user clicks desktop header / tablet header navigation link.
    */
    $('.nav__item a, .section-hero__button').click(function() {
        $.fn.smoothSectionScroll();
    });

    /*
        Accept Cookies.
    */
    if ($.cookie('accepted-cookies') !== 'active') {
        $('.cookies').show();
        }
    
        $('.cookies__button').on('click', function () {
        $.cookie('accepted-cookies', 'active', { expires: 1 });
        $('.cookies').fadeOut();
        });

    /*
        On page load.
    */
    $.fn.smoothSectionScroll();

});