<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use WP_Query;

class PropertyController extends Controller
{

    /**
     * List and filter properties.
     *
     * @param Request $request
     * @return WP_Query
     */
    public function index(Request $request)
    {
        $page = get_query_var('page');

        $properties = new WP_Query([
            'post_type' => 'properties',
            'paged' => ($page) ? $page : 1,
            'posts_per_page' => 10,
            's' => $request->location,
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => $request->for . '_price',
                    'value' => $request->max,
                    'type' => 'numeric',
                    'compare' => '<='
                ],
                [
                    'key' => 'bedrooms',
                    'value' => $request->bedrooms,
                    'compare' => '<='
                ],
            ]
        ]);

        return view('pages.properties.index', compact('properties'));
    }

    /**
     * Show property.
     *
     * @param string $slug
     * @return WP_Query
     */
    public function show($slug)
    {
        $property = new WP_Query([
            'name' => $slug,
            'post_type' => 'properties',
            'post_status' => 'publish',
        ]);

        return view('pages.properties.show', compact('property'));
    }
}
