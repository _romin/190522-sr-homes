<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use WP_Query;

class ArticleController extends Controller
{
    /**
     * List articles.
     *
     * @param Request $request
     * @return WP_Query
     */
    public function index(Request $request)
    {
        $page = get_query_var('page');

        $articles = new WP_Query([
            'post_type' => 'articles',
            'paged' => ($page) ? $page : 1,
            'posts_per_page' => 10,
        ]);

        return view('pages.articles.index', compact('articles'));
    }

    /**
     * Show articles.
     *
     * @param string $slug
     * @return WP_Query
     */
    public function show($slug)
    {
        $article = new WP_Query([
            'name' => $slug,
            'post_type' => 'articles',
            'post_status' => 'publish',
        ]);

        return view('pages.articles.show', compact('article'));
    }
}
