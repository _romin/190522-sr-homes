<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Taxonomy;
use Themosis\Support\Facades\PostType;

class CPT extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        PostType::make('articles', 'Articles', 'Article')
            ->setArguments([
                'menu_icon' => 'dashicons-welcome-widgets-menus',
                'supports' => ['title', 'editor', 'thumbnail'],
            ])
            ->set();

        PostType::make('properties', 'Properties', 'Property')
            ->setArguments([
                'menu_icon' => 'dashicons-admin-home',
                'supports' => ['title', 'editor'],
            ])
            ->set();

        PostType::make('testimonials', 'Testimonials', 'Testimonial')
            ->setArguments([
                'menu_icon' => 'dashicons-format-quote',
                'supports' => ['title'],
            ])
            ->set();

        Taxonomy::make('categories', ['articles'], 'Categories', 'Category')
            ->setArguments([
                'public' => true,
                'show_in_nav_menus'  => false,
                'hierarchical' => true,
            ])->set();
    }
}
