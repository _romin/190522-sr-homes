<?php

Route::get('properties', 'PropertyController@index')->name('property.index');
Route::get('properties/{post}', 'PropertyController@show')->name('property.show');


Route::get('articles', 'ArticleController@index')->name('articles.index');
Route::get('articles/{post}', 'ArticleController@show')->name('articles.show');


/**
 * Application routes.
 */
Route::any('front', function() {
    $testimonials = new WP_Query( [ 'post_type' => 'testimonials', 'posts_per_page' => -1 ] ); 

    return view('pages.front.index', compact('testimonials'));
});

Route::any('page', function () {
    return view('pages.default');
});

Route::fallback(function () {
    return view('pages.404.index');
});