# SR Homes and Lettings

A independent company providing estate & letting services.

## Installation

Install [Composer](https://getcomposer.org/doc/00-intro.md) to setup the project.

```
composer install
```

## Framework
Use [Themosis](https://getcomposer.org/doc/00-intro.md) framework to get familiar with the code structure.